package ru.vartanyan.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public void remove(@Nullable final E entity) throws Exception {
        if (entity != null) {
            removeById(entity.getId());
        }
    }

    @Override
    public void removeById(@NotNull final String id) throws Exception {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    protected abstract String getTableName();

    @Nullable
    public java.sql.Date convert(@Nullable final java.util.Date date) {
        if (date == null) return null;
        return new java.sql.Date(date.getTime());
    }

    @Nullable
    @Override
    public final E findById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable E session = fetch(resultSet);
        statement.close();
        return session;
    }

    protected abstract E fetch(ResultSet resultSet) throws SQLException;

    @NotNull
    @Override
    public List<E> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull ResultSet resultSet = statement.executeQuery();
        @NotNull List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
        statement.close();
    }

    public void addAll(final List<E> list) throws Exception {
        if (list == null) return;
        for (E entity: list) {
            try {
                add(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean contains(@NotNull final String id) throws SQLException {
        final Object object =  findById(id);
        return object != null;
    }

}
