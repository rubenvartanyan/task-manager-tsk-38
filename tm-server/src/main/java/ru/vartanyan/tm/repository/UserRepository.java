package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "APP_USER";
    }

    @Override
    protected User fetch(ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull User user = new User();
        user.setId(resultSet.getString("ID"));
        user.setLogin(resultSet.getString("LOGIN"));
        user.setPasswordHash(resultSet.getString("PASSWORD_HASH"));
        user.setEmail(resultSet.getString("EMAIL"));
        user.setFirstName(resultSet.getString("FIRST_NAME"));
        user.setLastName(resultSet.getString("LAST_NAME"));
        user.setMiddleName(resultSet.getString("MIDDLE_NAME"));
        user.setRole(Role.valueOf(resultSet.getString("ROLE")));
        user.setLocked(resultSet.getBoolean("LOCKED"));
        return user;
    }

    @Nullable
    @Override
    public final User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM `app_user` WHERE `login` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "DELETE FROM `app_user` WHERE `login` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        statement.execute();
        statement.close();
    }

    @Override
    public void add(@Nullable User user) throws Exception {
        if (user == null) return;
        @NotNull final String query =
                "INSERT INTO `app_user`(`id`, `login`, `locked`, `password_hash`, `email`, " +
                        "`first_name`, `last_name`, `middle_name`, `role`) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setBoolean(3, user.getLocked());
        statement.setString(4, user.getPasswordHash());
        statement.setString(5, user.getEmail());
        statement.setString(6, user.getFirstName());
        statement.setString(7, user.getLastName());
        statement.setString(8, user.getMiddleName());
        @Nullable Role role = user.getRole();
        statement.setString(9, role.toString());
        statement.execute();
    }

    @Override
    public void setPassword(@NotNull String password,
                            @NotNull String userId) throws SQLException {
        @NotNull final String query =
                "UPDATE `app_user` " +
                        "SET `password_hash` = ? " +
                        "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, password);
        statement.setString(2, userId);
        statement.execute();
    }

    @Override
    public void lockUser(@NotNull User user) throws SQLException {
        @NotNull final String query =
                "UPDATE `app_user` " +
                        "SET `locked` = ? " +
                        "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, true);
        statement.setString(2, user.getId());
        statement.execute();
    }

    @Override
    public void unlockUser(@NotNull User user) throws SQLException {
        @NotNull final String query =
                "UPDATE `app_user` " +
                        "SET `locked` = ? " +
                        "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, false);
        statement.setString(2, user.getId());
        statement.execute();
    }

    @Override
    public void updateUser(@NotNull User user) throws SQLException {
        @NotNull final String query =
                "UPDATE `app_user` " +
                        "SET `first_name` = ?, `last_name` = ?, `middle_name` = ?" +
                        "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, user.getFirstName());
        statement.setString(2, user.getLastName());
        statement.setString(3, user.getMiddleName());
        statement.setString(4, user.getId());
        statement.execute();
    }

}
