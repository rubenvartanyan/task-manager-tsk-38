package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.sql.SQLException;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(@NotNull final String userId) throws SQLException, EmptyIdException;

    @Nullable
    List<E> findAll(@NotNull final String userId) throws SQLException, EmptyIdException;

    @Nullable
    E findById(@NotNull final String id,
               @NotNull final String userId) throws SQLException, EmptyIdException;

    void removeById(@NotNull final String id,
                    @NotNull final String userId) throws SQLException, EmptyIdException;

    @Nullable
    E findOneByIndex(@NotNull Integer index,
                     @NotNull String userId) throws Exception;

    @Nullable
    E findOneByName(@NotNull final String name,
                    @NotNull final String userId) throws Exception;

    void removeOneByIndex(@NotNull final Integer index,
                          @NotNull final String userId) throws Exception;

    void removeOneByName(@NotNull final String name,
                         @NotNull final String userId) throws Exception;

    void updateEntityById(String id,
                      String name,
                      String description,
                      String userId) throws Exception;

    void updateEntityStatusById(
            @Nullable String userId,
            @Nullable Status status,
            @Nullable String id) throws SQLException, Exception;

}
