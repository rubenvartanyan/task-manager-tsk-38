package ru.vartanyan.tm.exception.system;

public class NullTaskException extends Exception {

    public NullTaskException() {
        super("Error! Task is not found...");
    }

}