package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.repository.ProjectRepository;
import ru.vartanyan.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService{

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }
    @NotNull
    @Override
    public final List<Task> findAllTaskByProjectId(@Nullable final String projectId,
                                                    @Nullable final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        final ITaskRepository taskRepository = new TaskRepository(connection);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void bindTaskByProjectId(@Nullable final String projectId,
                                    @Nullable final String taskId,
                                    @NotNull final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String projectId,
                                      @Nullable final String taskId,
                                      @NotNull final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.unbindTaskFromProject(projectId, taskId, userId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String projectId,
                                  @Nullable final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository taskRepository = new TaskRepository(connection);
            final IProjectRepository projectRepository = new ProjectRepository(connection);
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
