package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    public AbstractBusinessService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    public abstract IBusinessRepository<E> getRepository(@NotNull Connection connection);

    @Nullable
    @Override
    public E findById(@Nullable final String id,
                      @Nullable final String userId) throws EmptyIdException, SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        final IBusinessRepository<E> businessRepository = getRepository(connection);
        return businessRepository.findById(id, userId);
    }

    @Override
    public void clear(@Nullable final String userId) throws EmptyIdException, SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id,
                           @Nullable final String userId) throws EmptyIdException, SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.removeById(id, userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final Integer index,
                            @Nullable final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        final IBusinessRepository<E> businessRepository = getRepository(connection);
        return businessRepository.findOneByIndex(index, userId);
    }

    @Nullable
    @Override
    public E findOneByName(@Nullable final String name,
                           @Nullable final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        final IBusinessRepository<E> businessRepository = getRepository(connection);
        return businessRepository.findOneByName(name, userId);
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index,
                                 @Nullable final String userId) throws Exception{
        if (index < 0) throw new IncorrectIndexException(index);
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.removeOneByIndex(index, userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeOneByName(@Nullable final String name,
                                @Nullable final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.removeOneByName(name, userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public List<E> findAll(@Nullable final String userId) throws EmptyIdException, SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            List<E> list = businessRepository.findAll(userId);
            connection.commit();
            return list;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateEntityById(@Nullable final String id,
                                 @Nullable final String name,
                                 @Nullable final String description,
                                 @NotNull final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityById(userId, id, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateEntityByIndex(@NotNull final Integer index,
                                    @Nullable final String name,
                                    @Nullable final String description,
                                    @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityById(userId, entity.getId(), name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void startEntityById(@Nullable final String id,
                                @Nullable final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = findById(userId, id);
        if ( entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, Status.IN_PROGRESS, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void startEntityByName(@Nullable final String name,
                                  @Nullable final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, Status.IN_PROGRESS, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void startEntityByIndex(@NotNull final Integer index,
                                   @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, Status.IN_PROGRESS, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishEntityById(@Nullable final String id,
                                 @NotNull final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, Status.COMPLETE, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishEntityByName(@Nullable final String name,
                                   @NotNull final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, Status.COMPLETE, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishEntityByIndex(@NotNull final Integer index,
                                    @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, Status.COMPLETE, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateEntityStatusById(@Nullable final String id,
                                       @NotNull final Status status,
                                       @NotNull final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, status, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateEntityStatusByName(@Nullable final String name,
                                         @NotNull final Status status,
                                         @NotNull final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, status, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateEntityStatusByIndex(@NotNull final Integer index,
                                          @NotNull final Status status,
                                          @NotNull final String userId) throws Exception {
        if (index < 0) throw new IncorrectIndexException(index);
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> businessRepository = getRepository(connection);
            businessRepository.updateEntityStatusById(userId, status, entity.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
