package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.repository.ProjectRepository;

import java.sql.Connection;

public class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public IBusinessRepository<Project> getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    public void add(@NotNull final String name,
                    @NotNull final String description,
                    @NotNull final String userId) throws Exception {
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.add(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
