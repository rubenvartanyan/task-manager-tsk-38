package ru.vartanyan.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    final Connection connection = connectionService.getConnection();

    final IUserRepository userRepository = new UserRepository(connection);

    @After
    @SneakyThrows
    public void after() {
        connection.commit();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllUsersTest() throws Exception {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.addAll(users);
        Assert.assertNotNull(userRepository.findById(user1.getId()));
        Assert.assertNotNull(userRepository.findById(user2.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void addUserTest() throws Exception {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findById(user.getId()));
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void clearUsersTest() {
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findAllUsers() {
        userRepository.clear();
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        try {
            userRepository.addAll(users);
        } catch (ru.vartanyan.tm.exception.system.NullObjectException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException throwables) {
            throwables.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(2, userRepository.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findUserByIdTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findById(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeUserByIdTest() throws Exception {
        final User user1 = new User();
        userRepository.add(user1);
        final String userId = user1.getId();
        userRepository.removeById(userId);
        Assert.assertNull(userRepository.findById(userId));
    }

}
