package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.endpoint.Project;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectFindByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-find-by-id";
    }

    @Override
    public String description() {
        return "Find project by Id";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[FIND PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findProjectById(id, session);
        System.out.println(project.getName() + ": " + project.getDescription());
    }

}
